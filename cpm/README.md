CP/M OS Files
=============

### `2.2/`

`2.2/` contains the files from the distribution disk for the Xeros 1800
system, as found on the [Digital Research Binary Files][cpmz80-bin] page of
The Unofficial CP/M Web site (www.cpm.z80.de). The `READ.ME` file has been
removed as that was not part of the original distribution. (It points back
to the website.)

Note that the OS is not here; this is the "userland" programs only, though
there is a `CPM.SYS` that may contain the CCP and BDOS. `BIOS.ASM` contains
the Intel MDS-800 BIOS.



<!-------------------------------------------------------------------->
[cpmz80-bin]: http://www.cpm.z80.de/binary.html
